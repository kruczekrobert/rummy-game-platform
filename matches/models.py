from django.db import models
from django.contrib.auth.models import User


class Match(models.Model):
    winner = models.OneToOneField(User, on_delete=models.CASCADE, related_name='winner')
    loser = models.OneToOneField(User, on_delete=models.CASCADE, related_name='loser')
    points = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)

