from rest_framework import routers
from django.urls import path, include
from .views import MatchViewSet, UserViewSet

router = routers.DefaultRouter()
router.register(r'matches', MatchViewSet)
router.register('users', UserViewSet)

urlpatterns = [
    path('', include(router.urls))
]
