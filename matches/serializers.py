from rest_framework import serializers
from .models import Match
from django.contrib.auth.models import User


class MatchSerializer(serializers.HyperlinkedModelSerializer):
    # winner = serializers.ReadOnlyField(source='winner.username')
    # loser = serializers.ReadOnlyField(source='loser.username')

    class Meta:
        model = Match
        fields = ('winner', 'loser', 'points', 'created',)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'username')


