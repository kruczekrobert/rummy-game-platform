from rest_framework import viewsets
from .models import Match
from .serializers import MatchSerializer, UserSerializer
from django.contrib.auth.models import User


class MatchViewSet(viewsets.ModelViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

